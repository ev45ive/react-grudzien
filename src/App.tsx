import React, { Component } from "react";
import { PlaylistsView } from "./playlists/components/PlaylistsView";
import { MusicSearchView } from "./music/components/MusicSearchView";
import { DefaultLayout } from "./layouts/DefaultLayout";
import { Route, Switch, Redirect } from "react-router";

class App extends Component {
  render() {
    return (
      <DefaultLayout>


        <Switch>
          {/* / - Home */}
          <Redirect path="/" exact={true} to="/music" />

          <Route path="/music/:query?" component={MusicSearchView} />
          <Route path="/playlists" component={PlaylistsView} />

          {/* ** - PAGE NOT FOUND */}
          <Redirect path="**" to="/music" />
        </Switch>
      </DefaultLayout>
    );
  }
}

export default App;
