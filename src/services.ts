import { MusicSearch } from "./services/MusicSearch";
import { Security } from "./services/Security";
import React from "react";
import { Album } from "./models/Album";

export const security = new Security({
  auth_url: "https://accounts.spotify.com/authorize",
  response_type: "token",
  redirect_uri: "http://localhost:3000/",
  client_id: "224f8748135846bb8e4051449f2fe117"
});
export const musicSearch = new MusicSearch(
  "https://api.spotify.com/v1/search",
  security
);

export const MusicSearchContext = React.createContext({
  search(query: string):void {
    throw 'MIssing Music search provider!'
  },
  albums: [] as Album[]
});
