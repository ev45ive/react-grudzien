interface AuthConfig {
  auth_url: string;
  response_type: string;
  redirect_uri: string;
  client_id: string;
}

export class Security {
  constructor(private config: AuthConfig) {}

  token = "";

  authorize() {
    const {
      auth_url,
      redirect_uri,
      response_type = "token",
      client_id
    } = this.config;

    const url = `${auth_url}?redirect_uri=${redirect_uri}&response_type=${response_type}&client_id=${client_id}`;

    sessionStorage.removeItem('token')
    // Redirect to login page:
    location.replace(url);
  }

  getToken() {
    // Restore token
    this.token = JSON.parse(sessionStorage.getItem("token") || "null");

    if (!this.token && location.hash) {
      const match = location.hash.match(/access_token=([^&]*)/);
      this.token = (match && match[1]) || this.token;
      location.hash = ''

      // Save Token:
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}
