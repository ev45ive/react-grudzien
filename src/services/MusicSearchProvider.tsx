import { Component } from "react";
import { MusicSearchContext, musicSearch } from "../services";
import { Album } from "../models/Album";

type S = {
  albums: Album[];
  message: string;
};

export class MusicSearchProvider extends Component<{}, S> {

  
  state: S = {
    albums: [],
    message: ""
  };

  search = (query: string) => {
    musicSearch
      .searchAlbums(query)
      .then(albums =>
        this.setState({
          albums
        })
      )
      .catch((error: Error) =>
        this.setState({
          message: error.message
        })
      );
  };

  render() {
    return (
      <MusicSearchContext.Provider
        value={{
          search: this.search,
          albums: this.state.albums
        }}
      >
        {this.props.children}
      </MusicSearchContext.Provider>
    );
  }
}
