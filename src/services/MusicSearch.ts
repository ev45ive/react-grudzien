import axios, { AxiosError } from "axios";
import { Security } from "./Security";
import { AlbumsResponse } from "../models/Album";

export class MusicSearch {
  constructor(
    /** Search API url  */
    private api_url: string,
    private security: Security
  ) {}

  searchAlbums(query: string) {
    return axios
      .get<AlbumsResponse>(this.api_url, {
        headers: {
          Authorization: "Bearer " + this.security.getToken()
        },
        params: {
          type: "album",
          q: query
        }
      })
      .then(resp => {
        return resp.data.albums.items;
      })
      .catch((error: AxiosError) => {
        if (error.response!.status == 401) {
          this.security.authorize();
        }
        return Promise.reject(error.response!.data.error);
      });
  }
}
