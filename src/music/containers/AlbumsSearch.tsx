import { SearchForm } from "../components/SearchForm";
import { connect, MapDispatchToProps } from "react-redux";
import { AppState } from "../../store";
import { searchAlbums } from "../../reducers/search";
import { withRouter, RouteComponentProps } from "react-router";

// https://reacttraining.com/react-router/web/api/withRouter

export const AlbumsSearch = withRouter(
  connect(
    (state: AppState) => ({}),
    dispatch => {
      return {
        onSearch: searchAlbums(dispatch)
      };
    }
  )(SearchForm)
);
