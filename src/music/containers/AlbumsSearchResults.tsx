import { AppState } from "../../store";
import { connect } from "react-redux";
import { AlbumsGrid } from "../components/AlbumsGrid";

export const AlbumsSearchResults = connect(
  (state: AppState) => ({
    albums: state.search.results
  })

  // dispatch => ({})
)(AlbumsGrid);
