import React from "react";
import { Album } from "../../models/Album";

type P = {
  album: Album;
};

type S = {};

export class AlbumCard extends React.Component<P, S> {
  state = {};

  render() {
    return (
      <div className="card">
        <img className="card-img-top" src={this.props.album.images[0].url} />
        <div className="card-body">
          <h5 className="card-title">{this.props.album.name}</h5>
        </div>
      </div>
    );
  }
}
