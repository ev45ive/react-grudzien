import React from "react";
import { AlbumCard } from "./AlbumCard";
import "./AlbumsGrid.css";
import { Album } from "../../models/Album";

type P = {
  albums: Album[];
};

type S = {};

export class AlbumsGrid extends React.Component<P, S> {
  state = {};
  render() {
    return (
      <div className="card-group">
        {this.props.albums.map(album => (
          <AlbumCard album={album} key={album.id} />
        ))}
      </div>
    );
  }
}
