import React from "react";
import { AlbumsSearch } from "../containers/AlbumsSearch";
import { AlbumsSearchResults } from "../containers/AlbumsSearchResults";

export const MusicSearchView = (props: any) => {
  return (
    <div>
      <div className="row">
        <div className="col">
          <AlbumsSearch />
        </div>
      </div>

      <div className="row">
        <div className="col">
          <AlbumsSearchResults />
        </div>
      </div>
    </div>
  );
};
