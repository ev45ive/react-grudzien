import React from "react";
import { SearchField } from "../../playlists/components/SearchField";
import { RouteComponentProps } from "react-router";

type P = {
  onSearch(query: string): void;
} & RouteComponentProps<{ query?: string }>;

type S = {};

export class SearchForm extends React.Component<P, {}> {
  state = {};

  componentDidUpdate() {
    const query = this.props.match.params.query;
    if (query) {
      this.inputRef.current!.value = query;
      this.props.onSearch(query);
    }
  }

  search = (query: string) => {
    // this.props.history.push("/music/" + query);
    this.props.history.replace("/music/" + query);
    // this.props.history.push({
    //   pathname: "/music/" + query
    // });
  };

  inputRef = React.createRef<HTMLInputElement>();

  render() {
    return (
      <SearchField
        forwardRef={this.inputRef}
        onSearch={this.search}
        className="mb-3"
      />
    );
  }
}
