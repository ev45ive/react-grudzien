import React, { StatelessComponent } from "react";
import { Link, NavLink } from "react-router-dom";

type P = {};

export const DefaultLayout: StatelessComponent<P> = props => {
  return (
    <>
      <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
        <div className="container">
          <NavLink className="navbar-brand"  activeClassName="active" to="/">
            Music App
          </NavLink>

          <div className="collapse navbar-collapse">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" to="/music">
                  Search Music
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link disabled" to="/playlists">
                  Playlists
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container">
        <div className="row">
          <div className="col">
            <div>{props.children}</div>
          </div>
        </div>
      </div>
    </>
  );
};
