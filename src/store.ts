import { counter } from "./reducers/counter";
import playlists, { PlaylistsState } from "./reducers/playlists";
import { search, SearchState } from "./reducers/search";
import {
  createStore,
  Action,
  Reducer,
  AnyAction,
  combineReducers
} from "redux";

export interface AppState {
  counter: number;
  playlists: PlaylistsState;
  search: SearchState;
}

const rootReducer = combineReducers<AppState>({
  counter,
  playlists,
  search
});

export const store = createStore(rootReducer);

// store.dispatch(counterActions.increment(12));
//DEBUG:
(window as any).store = store;
