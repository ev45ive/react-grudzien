export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * HEX color
   */
  color: string;
  tracks?: Track[];
  
  // tracks: Array<Track>;
}

export interface Track {
  id: number;
  name: string;
}
