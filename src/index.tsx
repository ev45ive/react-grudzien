import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import "./index.css";

// Polyfills
import "core-js";
// import "core-js/es6/string";

(window as any).React = React;
(window as any).ReactDOM = ReactDOM;

// Get token before router changes url
security.getToken();

// Redux Store
import { store } from "./store";
import { Provider } from "react-redux";
store.subscribe(() => console.log(store.getState()));

// Routing
// import { HashRouter as Router } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";

// App
import App from "./App";

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <App />
    </Provider>
  </Router>,
  document.getElementById("root")
);

import * as serviceWorker from './serviceWorker';
import { security } from "./services";

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
