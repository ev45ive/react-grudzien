import { Playlist } from "../models/Playlist";
import { AnyAction, Reducer, Action, ActionCreator } from "redux";
import { AppState } from "../store";

export interface PlaylistsState {
  items: Playlist[];
  selectedId?: Playlist["id"];
}

const initialState: PlaylistsState = {
  items: [
    {
      id: 123,
      name: "React Hits!",
      favourite: true,
      color: "#ff00ff"
    },
    {
      id: 234,
      name: "React TOP20!",
      favourite: false,
      color: "#ffff00"
    },
    {
      id: 345,
      name: "Best of React",
      favourite: true,
      color: "#00ffff"
    }
  ]
};

/* Actions */
interface PLAYLISTS_SELECT extends Action<"PLAYLISTS_SELECT"> {
  payload: Playlist["id"];
}

interface PLAYLISTS_UPDATE extends Action<"PLAYLISTS_UPDATE"> {
  // payload: Partial<Playlist> & { id: Playlist["id"] };
  payload: Playlist;
}

type PlaylistsActions = PLAYLISTS_SELECT | PLAYLISTS_UPDATE;

/* Reducer */

export const playlists: Reducer<PlaylistsState, PlaylistsActions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "PLAYLISTS_SELECT":
      return {
        ...state,
        selectedId: action.payload
      };
    case "PLAYLISTS_UPDATE":
      return {
        ...state,
        items: state.items.map(item =>
          item.id === action.payload.id ? action.payload : item
        )
      };

    default:
      return state;
  }
};
export default playlists;

/* Action Creators */
export const selectPlaylist: ActionCreator<PLAYLISTS_SELECT> = (
  payload: Playlist["id"]
) => ({
  type: "PLAYLISTS_SELECT",
  payload
});

export const updatePlaylist: ActionCreator<PLAYLISTS_UPDATE> = (
  payload: Playlist
) => ({
  type: "PLAYLISTS_UPDATE",
  payload
});

(window as any).selectPlaylist = selectPlaylist;
(window as any).updatePlaylist = updatePlaylist;

/* Selectors */

export const selectSelectedPlaylist = (state: AppState) =>
  state.playlists.items.find(
    //
    item => item.id == state.playlists.selectedId
  );
