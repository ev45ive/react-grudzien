import { AnyAction, Reducer, Action, ActionCreator } from "redux";
import { Album } from "../models/Album";
import { musicSearch } from "../services";
import { Dispatch } from "react";

/* State */

export interface SearchState {
  results: Album[];
  query: string;
  isLoading: boolean;
  message: string;
}
// type SearchState = typeof initialState;

const initialState /* : SearchState */ = {
  results: [] as Album[],
  query: "",
  isLoading: false,
  message: ""
};

/* Actions */

interface SEARCH_START extends Action<"SEARCH_START"> {
  payload: {
    query: SearchState["query"];
  };
}

interface SEARCH_FAILED extends Action<"SEARCH_FAILED"> {
  payload: {
    error: Error["message"];
  };
}

interface SEARCH_SUCCESS extends Action<"SEARCH_SUCCESS"> {
  payload: {
    results: SearchState["results"];
  };
}

type Actions = SEARCH_START | SEARCH_FAILED | SEARCH_SUCCESS;

/* Reducer */
export const search: Reducer<SearchState, Actions> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case "SEARCH_START":
      return {
        ...state,
        isLoading: true,
        query: action.payload.query
      };

    case "SEARCH_SUCCESS":
      return {
        ...state,
        isLoading: false,
        results: action.payload.results
      };

    case "SEARCH_FAILED":
      return {
        ...state,
        isLoading: false,
        message: action.payload.error
      };

    default:
      return state;
  }
};

/* Action Creators */

const searchStart: ActionCreator<SEARCH_START> = (query: string) => ({
  type: "SEARCH_START",
  payload: { query }
});

const searchSuccess: ActionCreator<SEARCH_SUCCESS> = (results: Album[]) => ({
  type: "SEARCH_SUCCESS",
  payload: { results }
});

const searchFailed: ActionCreator<SEARCH_FAILED> = error => ({
  type: "SEARCH_FAILED",
  payload: { error }
});

export const searchAlbums = (dispatch: Dispatch<Actions>) => (
  query: string
) => {
  dispatch(searchStart(query));

  musicSearch
    .searchAlbums(query)
    .then(albums => dispatch(searchSuccess(albums)))

    .catch((error: Error) => dispatch(searchFailed(error.message)));
};

/* Selectors */
