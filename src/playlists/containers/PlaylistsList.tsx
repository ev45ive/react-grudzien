import { connect, MapStateToProps, MapDispatchToProps } from "react-redux";
import { FilteredItems } from "../components/FilteredItems";
import { AppState } from "../../store";
import { Item } from "../components/ItemsList";
import {
  selectSelectedPlaylist,
  selectPlaylist
} from "../../reducers/playlists";
import { Playlist } from "../../models/Playlist";

const withPlaylists = connect(

  // ComponentStateProps (from store)
  (state: AppState) => ({
    items: state.playlists.items,
    selected: selectSelectedPlaylist(state)
  }),

  // ComponentDispatchProps (from store)
  dispatch => ({
    onSelect: (playlist: Playlist) => {
      dispatch(selectPlaylist(playlist.id));
    }
  })
);

export const PlaylistsList = withPlaylists(FilteredItems);

/* 
<
  // ComponentStateProps (from store)
  {
    items: Item[];
    selected?: Item;
  },
  // ComponentDispatchProps (from store)
  {
    onSelect(item: Item): void;
  },
  // Component Own Props (from parent)
  {},
  // Store state
  AppState
> */
