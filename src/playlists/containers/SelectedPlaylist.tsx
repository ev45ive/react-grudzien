import { connect } from "react-redux";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { Playlist } from "../../models/Playlist";
import { AppState } from "../../store";
import { selectSelectedPlaylist, updatePlaylist } from '../../reducers/playlists';
import { bindActionCreators } from "redux";

const withPlaylist = connect<{ 
  selected?: Playlist; 
},{
  onSave(draft: Playlist): void 
},{},AppState>(
  
  (state) => ({
    selected: selectSelectedPlaylist(state)
  }),
  
  dispatch => bindActionCreators({
    onSave: updatePlaylist,
  },dispatch)
);

export const SelectedPlaylist = withPlaylist(
  (props: { 
    selected?: Playlist; 
    onSave(draft: Playlist): void 
  }) => {
    return props.selected ? (
      <PlaylistDetails onSave={props.onSave} playlist={props.selected} />
    ) : (
      <p>Please select playlist</p>
    );
  }
);
