import React from "react";
import { ListItem } from "./ListItem";

export type Item = {
  id: number;
  name: string;
};

type Props = {
  items: Item[];
  selected?: Item;
  onSelect(item: Item): void;
};

export class ItemsList extends React.PureComponent<Props> {
  render() {
    const selected = this.props.selected;

    return (
      <div className="list-group">
        {this.props.items.map(item => (
          <ListItem
            onSelect={this.props.onSelect}
            item={item}
            key={item.id}
            selected={selected && selected.id === item.id}
          />
        ))}
      </div>
    );
  }
}
