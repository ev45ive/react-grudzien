import React, { Fragment, ChangeEvent } from "react";
import { Playlist } from "../../models/Playlist";

type State = {
  playlist: Playlist;
  isEditing: boolean;
  remaining: number;
};

type Props = {
  playlist: Playlist;
  onSave(draft: Playlist): void;
};

export class PlaylistDetails extends React.PureComponent<Props, State> {
  state: State = {
    playlist: this.props.playlist,
    remaining: 0,
    isEditing: false
  };

  constructor(props: Props) {
    super(props);
    console.log("constructor");
  }

  // shouldComponentUpdate(nextProps:Props,nextState:State){

  //   return nextProps.playlist !== this.props.playlist
  //   || nextState.isEditing !== this.state.isEditing
  //   || nextState.playlist !== this.state.playlist
  // }

  // componentDidMount() {
  //   console.log("componentDidMount");
  // }

  // componentWillUnmount() {
  //   console.log("componentWillUnmount");
  // }

  // getSnapshotBeforeUpdate(x:any) {
  //   console.log("getSnapshotBeforeUpdate",x);
  //   return {
  //     scrollPos: 123
  //   };
  // }

  // componentDidUpdate(props:Props,state:State,snapshot: any) {
  //   console.log("componentDidUpdate", props, state, snapshot);
  // }

  static getDerivedStateFromProps(
    props: Props,
    nextState: State
  ): Partial<State> {
    return {
      playlist: nextState.isEditing ? nextState.playlist : props.playlist
    };
  }

  handleFieldChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    // event.persist()
    const target = event.target,
      fieldName = target.name,
      value = target.type == "checkbox" ? target.checked : target.value;

    this.setState((prevState: Readonly<State>, props: Readonly<{}>) => {
      return {
        playlist: {
          ...this.state.playlist,
          [fieldName]: value
        }
      };
    });
  };

  save = () => {
    this.props.onSave(this.state.playlist);
    this.setState({
      isEditing: false
    });
  };

  edit = () => {
    this.setState({
      isEditing: true
    });
  };

  cancel = () => {
    this.setState({
      isEditing: false,
      playlist: this.props.playlist
    });
  };

  render() {
    const playlist = this.state.playlist;

    return (
      <>
        {!this.state.isEditing && (
          <div>
            <h4>Playlist Details</h4>

            <dl>
              <dt>Name:</dt>
              <dd>{playlist.name}</dd>

              <dt>Favourite:</dt>
              <dd>{playlist.favourite ? "Yes" : "No"}</dd>

              <dt>Color:</dt>
              <dd
                style={{
                  color: playlist.color,
                  backgroundColor: playlist.color
                }}
              >
                {playlist.color}
              </dd>
            </dl>
            <input
              type="button"
              value="Edit"
              className="btn btn-info"
              onClick={this.edit}
            />
          </div>
        )}

        {this.state.isEditing ? (
          <div>
            <div className="form-group">
              <label>Name:</label>
              <input
                type="text"
                className="form-control"
                value={playlist.name}
                name="name"
                onChange={this.handleFieldChange}
              />
              {100 - this.state.playlist.name.length}
            </div>

            <div className="form-group">
              <label>Favourite:</label>
              <input
                type="checkbox"
                checked={playlist.favourite}
                name="favourite"
                onChange={this.handleFieldChange}
              />
            </div>

            <div className="form-group">
              <label>Color:</label>
              <input
                type="color"
                value={playlist.color}
                name="color"
                onChange={this.handleFieldChange}
              />
            </div>

            <input
              type="button"
              value="Cancel"
              className="btn btn-danger"
              onClick={this.cancel}
            />
            <input
              type="button"
              value="Save"
              className="btn btn-success"
              onClick={this.save}
            />
          </div>
        ) : null}
      </>
    );
  }
}
