import React, { DetailedHTMLProps, HTMLAttributes, RefObject } from "react";

type P = {
  onSearch(query: string): void;
  forwardRef?: RefObject<HTMLInputElement>;
  className: HTMLDivElement["className"];
};

type S = {};

export class SearchField extends React.Component<P, S> {
  state = {};
  inputRef = this.props.forwardRef || React.createRef<HTMLInputElement>();

  debounceHander?: NodeJS.Timeout;

  search = () => {
    // Reset delay
    this.debounceHander && clearTimeout(this.debounceHander)

    // Delay search
    this.debounceHander = setTimeout(() => {
      
      // Search
      if (this.inputRef.current) {
        this.props.onSearch(this.inputRef.current.value);
      }
    }, 400);
  };

  componentWillUnmount(){
    this.debounceHander && clearTimeout(this.debounceHander)
  }

  componentDidMount() {
    if (this.inputRef.current) {
      this.inputRef.current.focus();
    }
  }

  render() {
    return (
      <div className={`input-group ${this.props.className}`}>
        <input
          className="form-control"
          ref={this.inputRef}
          placeholder="Search"
          onChange={this.search}
        />
        <input
          type="button"
          value="Search"
          className="btn"
          onClick={this.search}
        />
      </div>
    );
  }
}

// const SearchInput = React.forwardRef((props, ref) => (
//   <div>
//     <input type="text" ref={ref} />
//   </div>
// ));

// <SearchInput ref={refObj} />;

// refObj.current == HTMLInputElement;
