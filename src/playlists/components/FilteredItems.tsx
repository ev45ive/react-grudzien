import { ItemsList, Item } from "./ItemsList";
import { SearchField } from "./SearchField";
import React from "react";

type P = {
  items: Item[];
  selected?: Item;
  onSelect(item: Item): void;
};

type S = {
  query: string;
};

export class FilteredItems extends React.PureComponent<P, S> {
  state = {
    query: ""
  };

  search = (query: string) => {
    this.setState({ query });
  };

  inputRef = React.createRef<HTMLInputElement>();
  searchRef = React.createRef<SearchField>();

  componentDidMount() {
    console.log(this);
  }

  render() {
    const filteredItems = this.props.items.filter(item =>
      item.name.toLowerCase().includes(this.state.query.toLowerCase())
    );

    return (
      <div>
        <SearchField
          className="mb-3"
          ref={this.searchRef}
          forwardRef={this.inputRef}
          onSearch={this.search}
        />
        <ItemsList
          items={filteredItems}
          selected={this.props.selected}
          onSelect={this.props.onSelect}
        />
      </div>
    );
  }
}
