import React from "react";

type Props = {
  selected?: boolean;
  item: { id: number; name: string };
  onSelect(item: { id: number }): void;
};

export class ListItem extends React.Component<Props> {
  
  onSelect = () => {
    this.props.onSelect(this.props.item);
  }

  render() {
    return (
      <div
        className={`list-group-item ${this.props.selected ? "active" : ""}`}
        onClick={this.onSelect}
      >
        {this.props.item.name}
      </div>
    );
  }
}
