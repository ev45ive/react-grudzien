import React from "react";

import { PlaylistsList } from "../containers/PlaylistsList";
import { SelectedPlaylist } from "../containers/SelectedPlaylist";

// export class PlaylistsView extends React.Component {
//   render() {
//     return
export const PlaylistsView = () => (
  <div className="row">
    <div className="col">
      <PlaylistsList />
    </div>
    <div className="col">
      <SelectedPlaylist />
    </div>
  </div>
);
//   }
// }
